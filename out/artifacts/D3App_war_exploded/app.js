var app = angular.module("graphApp", []);

app.controller("AppCtrl", function ($scope) {

    $scope.init = function () {

        var nodes = {};

        var links = $scope.links;
        console.log("links inside init: "+links);

        links.forEach(function (link) {
            link.source = nodes[link.source] || (nodes[link.source] = {
                name: link.source
            });
            link.target = nodes[link.target] || (nodes[link.target] = {
                name: link.target,
                stat: link.status
            });
        });

        var width = 960,
            height = 400;

        var force = d3.layout.force()
                .nodes(d3.values(nodes))
                .links(links)
                .size([width, height])
                .linkDistance(60)
                .charge(-3000)
                .on("tick", tick)
        /*.start()*/;

        d3.select("svg")
            .remove();

        var svg = d3.select("#graph").append("svg")
            .attr("width", width)
            .attr("height", height);

        var link = svg.selectAll(".link")
            .data(force.links())
            .enter().append("line")
            .attr("class", "link");

        var node = svg.selectAll(".node")
            .data(force.nodes())
            .enter().append("g")
            .attr("class", function (d) {
                if (d.stat == 200) {
                    return "node greenStyle";
                } else if (d.stat == 404) {
                    return "node redStyle";
                }
                console.log(d.stat + "   " + d.name);
                return "node";
            })
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .call(force.drag);

        node.append("circle")
            .attr("r", 15);

        node.append("text")
            .attr("x", 22)
            .attr("dy", ".35em")
            .text(function (d) {
                return d.name;
            });

        force.start();
        for (var i = 0; i < 100; ++i) force.tick();
        force.stop();

        function tick() {
            link
                .attr("x1", function (d) {
                    return d.source.x;
                })
                .attr("y1", function (d) {
                    return d.source.y;
                })
                .attr("x2", function (d) {
                    return d.target.x;
                })
                .attr("y2", function (d) {
                    return d.target.y;
                });

            node
                .attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });
        }

        function mouseover() {
            /*d3.select(this).select("circle").transition()
                .duration(750)
                .attr("r", 30);*/
        }

        function mouseout() {
            /*d3.select(this).select("circle").transition()
                .duration(750)
                .attr("r", 20);*/
        }


    };

    $scope.drawGraph = function() {
        console.log($scope.graphData);
        $scope.links = eval('(' + $scope.graphData + ')');
        $scope.init();
    };

    $scope.graphData = "[{source: \"Microsoft\", target: \"Amazon\", status: \"200\"},\n"+
    "{source: \"Microsoft\", target: \"HTC\", status: \"404\"},\n"+
    "{source: \"Microsoft\", target: \"Other\", status: \"200\"},\n"+
    "{source: \"Microsoft\", target: \"Apple\", status: \"200\"},\n"+
    "{source: \"Apple\", target: \"Sony\", status: \"404\"},\n"+
    "{source: \"Apple\", target: \"HTC\", status: \"404\"}"+
    "]";
    $scope.links = eval('(' + $scope.graphData + ')');

});





